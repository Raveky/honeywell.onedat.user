using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using HoneyWell.OneDAT.Commons.Integration;
using HoneyWell.OneDAT.Commons.Integration.Interfaces;
using HoneyWell.OneDAT.Commons.Interfaces;
using HoneyWell.OneDAT.User.WorkerService.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace HoneyWell.OneDAT.User.WorkerService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var configuration = GetConfiguration();
            UserDATService uds = new UserDATService(configuration);
            CreateHostBuilder(configuration, uds, args).Build().Run();
        }

        private static IConfiguration GetConfiguration()
        {

            var builder = new ConfigurationBuilder()

                .SetBasePath(Directory.GetCurrentDirectory())

                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)

                .AddEnvironmentVariables();

            return builder.Build();

        }

        public static IHostBuilder CreateHostBuilder(IConfiguration configuration, IOneDATService service, string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<Worker>();
                    services.AddSingleton<IMessageService, MessageService>(serviceProvider => new MessageService(configuration, service));

                });
    }
}
