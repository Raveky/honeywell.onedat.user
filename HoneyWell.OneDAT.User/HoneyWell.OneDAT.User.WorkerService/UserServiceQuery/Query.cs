﻿using HoneyWell.OneDAT.Commons.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace HoneyWell.OneDAT.User.WorkerService.UserServiceQuery
{
    public class Query : IQuery
    {
        private string _Query = null;
        public string GetQuery(string queryName)
        {
            switch (queryName.ToUpper())
            {
                case "UOMTEMPLATEID":
                    return _Query = @"select UOM_TEMPLATE_ID from sc_user.APPLICATION_USER";

                default:
                    return _Query;
            }
           
        }

        public string GetQuery()
        {
            throw new NotImplementedException();
        }
    }
}
